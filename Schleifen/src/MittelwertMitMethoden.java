import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		
		int anzahl;
		double zahl;
		double ergebnis = 0;
		double m = 0;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert von beliebig vielen Zahlen. Wieviele Zahlen m�chten Sie eingeben?");
		anzahl = myScanner.nextInt();

		for (int i = 1; i <= anzahl; i++) {
			zahl = eingabe(myScanner, "Bitte geben Sie die Zahl " + i + " ein: ");
			ergebnis += zahl;
		}
	
		System.out.println("Der Mittelwert betr�gt: " + mittelwertBerechnung(ergebnis, anzahl));
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double ergebnis, int anzahl) {
		double m = ergebnis/anzahl;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}
