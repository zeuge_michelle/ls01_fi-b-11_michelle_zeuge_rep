import java.util.Scanner;

class FahrkartenautomatSchleifen
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       
       while(true) {
    	   float zuZahlenderBetrag = 0.0f; 
    	   float r�ckgabebetrag;
       
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
    	   r�ckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben(r�ckgabebetrag);
       }
       
       
    }
   
    public static float fahrkartenbestellungErfassen(Scanner tastatur) {
        
    	float einzelpreis = 0.0f;
        float zuZahlenderBetrag = 0.0f;
        int wahl = 0;
        
    	
        System.out.println("Fahrkartenbestellvorgang:\n=========================\n");
        System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
        System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
    	
        
        while(wahl != 1 && wahl != 2 && wahl !=3) {
        	
        	System.out.print("Ihre Wahl: ");
        	wahl = tastatur.nextInt();
            
        	if(wahl == 1) {
        		einzelpreis = 2.90f;
        	}
        	else if(wahl == 2) {
        		einzelpreis = 8.60f;
        	}
        	else if(wahl == 3) {
        		einzelpreis = 23.50f;
        	}
        	else {
        		System.out.println(">> Falsche Eingabe <<");
        	}
        }
        
        
        System.out.print("Anzahl der Tickets: ");
        byte anzahlTickets = tastatur.nextByte();
        
        //Ticketgrenze
        while(anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.println("Bitte beachten Sie, dass nur 1 bis 10 Tickets gekauft werden k�nnen.");
        	
        	  System.out.print("Anzahl der Tickets: ");
              anzahlTickets = tastatur.nextByte();
        }
        
        zuZahlenderBetrag = einzelpreis * anzahlTickets;
    	
    	return zuZahlenderBetrag;
    }
    
    public static float fahrkartenBezahlen(Scanner tastatur, float zuZahlenderBetrag) {
    	
    	// Geldeinwurf
        // -----------
        float eingezahlterGesamtbetrag = 0.0f;
   
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   float eingeworfeneM�nze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	// Fahrscheinausgabe
        // -----------------
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		 }
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(float r�ckgabebetrag){
    	
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
     	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
            if (r�ckgabebetrag !=0) // 5 Cent Ausgabe, Inas Fix zum Thema
            { 
         	   System.out.println("5 CENT");
         	   r�ckgabebetrag = 0.00f;
            }
         	
            System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\r\n" + "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
        }

    	
    }

}