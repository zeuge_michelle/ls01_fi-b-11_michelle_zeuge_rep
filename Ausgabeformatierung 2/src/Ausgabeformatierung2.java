
public class Ausgabeformatierung2 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		System.out.println("\nAufgabe 1\n");
		
		System.out.printf("\n%10s", "**");
		System.out.printf("\n%6s ", "*");
		System.out.printf("%6s", "*");
		System.out.printf("\n%6s ", "*");
		System.out.printf("%6s", "*");
		System.out.printf("\n%10s\n", "**");
		
		
		//Aufgabe 2
		System.out.println("\nAufgabe 2\n");
		
		System.out.printf("\n%-5s", "0!");
		System.out.printf("%-20s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "1");
		
		System.out.printf("\n%-5s", "1!");
		System.out.printf("%-20s", "= 1");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "1");
		
		System.out.printf("\n%-5s", "2!");
		System.out.printf("%-20s", "= 1 * 2");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "2");

		System.out.printf("\n%-5s", "3!");
		System.out.printf("%-20s", "= 1 * 2 * 3");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "6");
		
		System.out.printf("\n%-5s", "4!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "24");
		
		System.out.printf("\n%-5s", "5!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf("%s", "=");
		System.out.printf("%5s", "120");
		
		System.out.println("\n");
		//Zeilenumbruch zur Formatierung
		
		//Aufgabe 3
		
		System.out.println("\nAufgabe 3\n");
		
		String f = "Fahrenheit";
		String c = "Celsius";
		
		int tempf1 = -20;
		int tempf2 = -10;
		int tempf3 = 0;
		int tempf4 = 20;
		int tempf5 = 30;
		
		double tempc1 = -28.8889;
		double tempc2 = -23.3333;
		double tempc3 = -17.7778;
		double tempc4 = -6.6667;
		double tempc5 = -1.1111;
		
		System.out.printf("\n%-12s", f);
		System.out.printf("| %10s", c);
		
		System.out.printf("\n------------------------");
		
		System.out.printf("\n%-12d", tempf1);
		System.out.printf("| %10.2f", tempc1);
		
		System.out.printf("\n%-12d", tempf2);
		System.out.printf("| %10.2f", tempc2);
		
		System.out.printf("\n%+-12d", tempf3);
		System.out.printf("| %10.2f", tempc3);
		
		System.out.printf("\n%+-12d", tempf4);
		System.out.printf("| %10.2f", tempc4);
		
		System.out.printf("\n%+-12d", tempf5);
		System.out.printf("| %10.2f", tempc5);
		
	}

}
