import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		
		//Deklaration von Variablen
		double zahl1;
		double zahl2;
		double m;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		
		zahl1 = eingabe(tastatur, "Bitte geben Sie die erste Zahl ein: ");
		zahl2 = eingabe(tastatur, "Bitte geben Sie die zweite Zahl ein: ");
		
		m = berechneMittelwert(zahl1, zahl2);
		
		ausgabe(m);
		
		tastatur.close();
	}
	
	//Methode zur Eingabe
	public static double eingabe(Scanner ms, String text) {
	
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	
	}
	
	//Methode zur Berechnung des Mittelwertes
	public static double berechneMittelwert(double zahl1, double zahl2) {
		
		double m = (zahl1 + zahl2) / 2;
		return m;
		//return (zahl1 + zahl2) / 2;
	}
	
	//Methode zur Ausgabe
	public static void ausgabe(double mittelwert) {
		
		System.out.println("Mittelwert: " + mittelwert);
		
	}
	
}
