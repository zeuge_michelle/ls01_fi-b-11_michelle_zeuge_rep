import java.util.Scanner;

public class KonsoleneingabeUebung {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		
		String name = "";
		float zahl1 = 0.0f, zahl2 = 0.0f, ergebnis = 0.0f;
		
		int zahlInt;
		double zahlDouble;
		long zahlLong;
		byte zahlByte;
		short zahlShort;
		boolean wahrheitswert;
		char zeichen;
		
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		name = eingabe.next();
		System.out.println("Hallo " + name + "!\n");
		
		System.out.println("Gleitkommazahl Rechnung");
		
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		zahl1 = eingabe.nextFloat();
		
		System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = eingabe.nextFloat();
		
		ergebnis = zahl1 + zahl2;
		System.out.println("Das Ergebnis der Berechnung ist: " + ergebnis + "\n");
		
		
		System.out.print("Double: ");
		zahlDouble = eingabe.nextDouble();
		System.out.println(zahlDouble);
		
		System.out.print("Long: ");
		zahlLong = eingabe.nextLong();
		System.out.println(zahlLong);
		
		System.out.print("Byte: ");
		zahlByte = eingabe.nextByte();
		System.out.println(zahlByte);
		
		System.out.print("Short: ");
		zahlShort = eingabe.nextShort();
		System.out.println(zahlShort);
		
		System.out.print("Boolean: ");
		wahrheitswert = eingabe.nextBoolean();
		System.out.println(wahrheitswert);
		
		System.out.print("Char: ");
		zeichen = eingabe.next().charAt(0);
		System.out.println(zeichen);
		
		System.out.print("Char: ");
		zeichen = eingabe.next().charAt(3);
		System.out.println(zeichen);
		
		eingabe.close();
	}

}
