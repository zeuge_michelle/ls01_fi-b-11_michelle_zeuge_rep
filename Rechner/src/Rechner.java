import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2;  
     
    System.out.print("\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
    
    
    //AB Konsoleneingabe Aufgabe 1
    
    //Subtraktion
    System.out.println("\n\n***Subtraktion***");
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
    int zahl3 = myScanner.nextInt();
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
    int zahl4 = myScanner.nextInt();
   
    int ergebnisSub = zahl3 - zahl4;
    
    System.out.print("\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl3 + " - " + zahl4 + " = " + ergebnisSub); 
    
  //Multiplikation
    System.out.println("\n\n***Multiplikation***");
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
    int zahl5 = myScanner.nextInt();
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
    int zahl6 = myScanner.nextInt();
   
    int ergebnisMult = zahl5 * zahl6;
    
    System.out.print("\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl5 + " x " + zahl6 + " = " + ergebnisMult);
    
  //Division
    System.out.println("\n\n***Division***");
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
    int zahl7 = myScanner.nextInt();
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
    int zahl8 = myScanner.nextInt();
   
    int ergebnisDiv = zahl7 / zahl8;
    
    System.out.print("\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl7 + " : " + zahl8 + " = " + ergebnisDiv);
    
    myScanner.close();
     
  }    
}