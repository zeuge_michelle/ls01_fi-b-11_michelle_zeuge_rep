﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       //ändern von double in float, weil double zu genau, und dadurch unterschlägt er Geld bei ausgabe
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       byte anzahlTickets;

       //System.out.print("Zu zahlender Betrag (EURO): "); Eingabe des Preises vorher
       System.out.print("Ticketspreis (Euro): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       
       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextByte();
       

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       
       zuZahlenderBetrag = anzahlTickets * zuZahlenderBetrag;
       //zuZahlenderBetrag *= anzahlTickets; als verkürzte Form
       
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {

    	   System.out.printf("Noch zu zahlen: %.2f EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag);
    	   //System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           if (rückgabebetrag !=0) // 5 Cent Ausgabe, Inas Fix zum Thema
           { 
        	   System.out.println("5 CENT");
        	   rückgabebetrag = 0.00f;
           }
        	   
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       tastatur.close();
       //Eclipse hat Warnung gegeben, dass Scanner nie geschlossen wurde
    }
}

/**
 * 5. Begründen Sie ihre Entscheidung für die Wahl des Datentyps.
 * 
Ich habe mich für byte entschieden, da ich davon ausgehe, dass an einem normalen Ticketautomat niemand mehr als 127 Tickets auf einmal kaufen wird. 
Außerdem ist eine Anzahl von etwas in der reellen Welt immer eine Ganzzahl, da ja niemand ein halbes Ticket kauft.
Der kleinste Datentyp für Ganzzahlen ist byte, also ist das die sinnvollste Wahl.

6. Erläutern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl* einzelpreis passiert.

Es wird die zuvor eingegebene Anzahl der Tickets mit dem zuvor eingegebenen Preis multipliziert.
Das heißt wenn man für die Anzahl 2 eingibt und für den Einzelpreis 2,5, 
dann wird das Produkt dieser beiden Werte ausgegeben, was in diesem Falle dann 5 wäre.
Genauer gesagt wird im Arbeitsspeicher für die Variable anzahl ein ganzzahliger Wert gespeichert, 
nachdem er eingegeben wurde, genauso für einzelpreis, wobei dort eine Gleitkommazahl abgefragt wird. 
Diese beiden Werte werden aufgerufen und mit dem Operator * verbunden, 
wodurch ein Produkt in eine Variable gespeichert wird.
 */


