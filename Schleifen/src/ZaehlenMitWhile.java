import java.util.Scanner;

public class ZaehlenMitWhile {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie einen Wert f�r n ein: ");
		int n = tastatur.nextInt();
		int i = 1;
		
		while (i <= n) {
			if (i != n) {
				System.out.print(i + ", ");
			}
			else {
				System.out.println(i);
			}
			i++;
		}
		//R�ckw�rts
		i = n;
		
		while (i >= 1) {
			if (i != 1) {
				System.out.print(i + ", ");
			}
			else {
				System.out.println(i);
			}
			i--;
		}

		
	}

}
