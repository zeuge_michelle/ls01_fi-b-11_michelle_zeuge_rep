/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 25.10.2021
  * @author << Michelle Zeuge >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 250000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3766082;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 8355;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 190000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f;
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Einwohner Berlin: " + bewohnerBerlin);
    
    System.out.println("Alter in Tage: " + alterTage);
    
    System.out.println("Gewicht des gr��ten Tiers der Weltcin kg: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes der Welt in km�: " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes der Welt in km�: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

