import java.util.Scanner;

public class KonsoleneingabeUebung {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		
		String name = "";
		byte alter;
		
		System.out.print("Willkommen Benutzer! Bitte geben Sie Ihren Namen ein: ");
		name = eingabe.next();
		System.out.println("\nHallo " + name + "!\n");
		
		System.out.print("Bitte geben Sie Ihr Alter an: ");
		alter = eingabe.nextByte();
		System.out.println("\n...Eingabe gespeichert...\n\nBenutzer " + name + " ist " + alter + " Jahre alt.");
		
		eingabe.close();

	}

}
