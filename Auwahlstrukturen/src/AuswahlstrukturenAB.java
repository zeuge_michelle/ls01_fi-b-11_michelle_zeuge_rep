import java.util.Scanner;


public class AuswahlstrukturenAB {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		//AUFGABE 4
		System.out.println("Bitte geben Sie den Bestellwert ein: ");
		float bestellwert = tastatur.nextFloat();
		float rechnungswert;
		
		if (bestellwert <= 100) {
			rechnungswert = bestellwert * 0.9f;
			System.out.println("Sie erhalten 10% Rabatt!");
		}
		else if (bestellwert <= 500) {
			rechnungswert = bestellwert * 0.85f;
			System.out.println("Sie erhalten 15% Rabatt!");
		}
		else {
			rechnungswert = bestellwert * 0.8f;
			System.out.println("Sie erhalten 20% Rabatt!");
		}
		
		float endwert = rechnungswert * 1.19f;
		System.out.printf("Der Bestellwert inklusive Rabatt und Mehrwertsteuer betr�gt: %.2f ", endwert);
		
	}

}
