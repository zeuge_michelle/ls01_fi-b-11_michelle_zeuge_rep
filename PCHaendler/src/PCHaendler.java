import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		String artikel;
		int anzahl;
		double preis;
		double mwst;
		
		//Eingaben
		artikel = liesString(tastatur, "Was m�chten Sie bestellen? ");
		anzahl = liesInt(tastatur, "Geben Sie die Anzahl ein: ");
		preis = liesDouble(tastatur, "Geben Sie den Nettopreis ein: ");
		mwst = liesDouble(tastatur, "Geben Sie den Mehrwertsteuersatz in Prozent ein: "); 
	
		//Verarbeitung
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		//Ausgabe
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		tastatur.close();
		
	}
	
	
	public static String liesString(Scanner ms, String text) {
		
		System.out.print(text);
		String eingabeText = ms.next();
		return eingabeText;
		
	}
	
	public static int liesInt(Scanner ms, String text) {
		
		System.out.print(text);
		int ganzzahl = ms.nextInt();
		return ganzzahl;
		
	}
	
	public static double liesDouble(Scanner ms, String text) {
		
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double 
	nettopreis) {
		
		double nettogesamtpreis = anzahl * nettopreis; 
		return nettogesamtpreis;
		
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, 
	double mwst) {
		
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
		
	}
	 
	public static void rechungausgeben(String artikel, int anzahl, double 
	nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
	
}
