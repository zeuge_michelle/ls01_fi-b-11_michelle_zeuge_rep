import java.util.Scanner;

class FahrkartenautomatMitMethoden
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneM�nze;
       float r�ckgabebetrag;
       byte anzahlTickets;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       r�ckgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(r�ckgabebetrag);
       
       tastatur.close();
       
    }
   
    public static float fahrkartenbestellungErfassen(Scanner tastatur) {
    	
    	System.out.print("Ticketspreis (Euro): ");
        float zuZahlenderBetrag = tastatur.nextFloat();
        
        //bei negativen Ticket Wert, wird Hinweis ausgegeben
        if(zuZahlenderBetrag < 0) {
        	System.out.println("Bitte beachten Sie, dass der Ticketpreis nicht negativ sein kann. Der Ticketpreis wurde auf 1 � gesetzt.");
        	zuZahlenderBetrag = 1f;
        }
        
        System.out.print("Anzahl der Tickets: ");
        byte anzahlTickets = tastatur.nextByte();
        
        //Ticketgrenze
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.println("Bitte beachten Sie, dass nur 1 bis 10 Tickets gekauft werden k�nnen. Die Anzahl der Tickets wurde auf 1 gesetzt.");
        	anzahlTickets = 1;
        }
        
        zuZahlenderBetrag *= anzahlTickets;
    	
    	return zuZahlenderBetrag;
    }
    
    public static float fahrkartenBezahlen(Scanner tastatur, float zuZahlenderBetrag) {
    	
    	// Geldeinwurf
        // -----------
        float eingezahlterGesamtbetrag = 0.0f;
   
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f EURO", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   float eingeworfeneM�nze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	// Fahrscheinausgabe
        // -----------------
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		 }
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(float r�ckgabebetrag){
    	
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
     	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
            if (r�ckgabebetrag !=0) // 5 Cent Ausgabe, Inas Fix zum Thema
            { 
         	   System.out.println("5 CENT");
         	   r�ckgabebetrag = 0.00f;
            }
         	   
        }

    	
    }

}
