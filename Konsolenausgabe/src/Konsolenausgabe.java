
public class Konsolenausgabe {

	public static void main(String[] args) {
		System.out.println("Hallo! \nwie gehts dir?");
		//Zeilenumbruch
		System.out.println("Hallo! \b wie gehts dir?");
		//Backspace BS
		System.out.println("Hallo! \fwie gehts dir?");
		//Form Feed FF
		System.out.println("Hallo! \twie gehts dir?");
		//Horizontal Tab HAT
		System.out.println("Hallo! \rwie gehts dir?");
		//Carriage Return CR
		System.out.println("Hallo! \\wie gehts dir?");
		//Backslash \
		System.out.println("Hallo! \"wie gehts dir?");
		//Anf�hrungszeichen "
		System.out.println("Hallo! \'wie gehts dir?");
		//Hochkomma '
		
		
		//String Befehle
		String z = "Java-Programm";
		//Deklaration einer Variable
		
		System.out.printf( "\n|%s|\n", z);
		//das s sagt, es erwartet eine Zeichenkette
		
		System.out.printf( "\n|%18s|\n", z);
		//die 18 h�lt 18 Stellen frei (18 bytes?), rechtsb�ndig
		
		System.out.printf( "\n|%-18s|\n", z);
		//linksb�ndig
		
		System.out.printf( "\n|%20.4s|\n", z);
		//hat 20 Stellen, aber nur 4 werden Ausgegeben
		
		System.out.printf( "\n|%-20.4s|\n", z);
		//das gleiche nur linksb�ndig
		
		int n = 123;
		//integer, ganzzahlig
		
		System.out.printf("\n%d %d", n, n);
		//d steht f�r ganze Zahl
		
		System.out.printf("\n|%15d| |%15d|", n, n);
		
		System.out.printf("\n|%d| |%d|", n, -n);
		//Vorzeichen -
		
		double k = 12.9374856;
		//Kommazahl wird mit . getrennt
		
		System.out.printf("\n%f\n", k);
		//f f�r Kommazahlen
		
		//Strg + 7 zum auskommentieren
		
		System.out.println("\n");
		//nur ein zus�tzlicher Zeilenumbruch aus optischen Gr�nden
		
		//Konsolenausgabe �bung 1 
		//Aufgabe 1
		
		System.out.print("Das Wetter ist sch�n.");
		System.out.print(" Wie findest du das Wetter?");
		
		
		System.out.println("\nWelche Klasse bist du? \nIch bin in der Klasse \"FI-B-11\".");
		//Benutzung von Escape-Symbolen
		
		//print gibt etwas hintereinander weg aus, w�hrend println einen Zeilenumbruch macht, 
		//da es eine Zeile (Line) schreibt
		
		//Aufgabe 2
		
		System.out.printf("\n%7s", "*");
		System.out.printf("\n%8s", "***");
		System.out.printf("\n%9s", "*****");
		System.out.printf("\n%10s", "*******");
		System.out.printf("\n%11s", "*********");
		System.out.printf("\n%12s", "***********");
		System.out.printf("\n%13s", "*************");
		System.out.printf("\n%8s", "***");
		System.out.printf("\n%8s", "***");
		
		System.out.println("\n");
		//nur ein zus�tzlicher Zeilenumbruch aus optischen Gr�nden
		
		//Aufgabe 3
		
		double zahl1 = 22.4234234;
		double zahl2 = 111.2222 ;
		double zahl3 = 4.0;
		double zahl4 = 1000000.551;
		double zahl5 = 97.34;
	
		System.out.printf("%.2f\n", zahl1);
		System.out.printf("%.2f\n", zahl2);
		System.out.printf("%.2f\n", zahl3);
		System.out.printf("%.2f\n", zahl4);
		System.out.printf("%.2f\n", zahl5);
		
	}

}
